// Import the standard library
local lib = import 'gmailctl.libsonnet';

local me = 'alex@gitlab.com';
local toMe = { to: me };

{
  version: 'v1alpha3',
  author: {
    name: 'Alex Hanselka',
    email: me,
  },
  // Note: management is optional. If you prefer to use the
  // GMail interface to add and remove labels, you can safely remove
  // this section of the config.
  labels: [
    {
      name: 'Cron',
    },
    {
      name: 'Sent Messages',
    },
    {
      name: 'Sentry',
    },
    {
      name: 'OpsGitLabInstance',
    },
    {
      name: 'DigitalOcean',
    },
    {
      name: 'Forseti',
    },

    {
      name: 'Ops Contact',
    },
    {
      name: 'Patching',
    },
  ],
  rules: [
    {
      filter: {
        and: [
          {
            from: 'cron',
          },
          {
            query: 'list:"<ops-contact.gitlab.com>"',
          },
        ],
      },
      actions: {
        archive: true,
        markImportant: false,
        labels: [
          'Cron',
        ],
      },
    },
    {
      filter: {
        from: 'sentry@mg.gitlab.com',
      },
      actions: {
        archive: true,
        markSpam: false,
        markImportant: false,
        labels: [
          'Sentry',
        ],
      },
    },
    {
      filter: {
        from: 'support@digitalocean.com',
      },
      actions: {
        labels: [
          'DigitalOcean',
        ],
      },
    },
    {
      filter: {
        to: "'ops-contact+forseti@gitlab.com'",
        isEscaped: true,
      },
      actions: {
        archive: true,
        markImportant: false,
        labels: [
          'Forseti',
        ],
      },
    },
    {
      filter: {
        to: "'ops-contact+do-gb@gitlab.com'",
        isEscaped: true,
      },
      actions: {
        archive: true,
        labels: [
          'DigitalOcean',
        ],
      },
    },
    {
      filter: {
        from: 'google-my-business-noreply@google.com',
      },
      actions: {
        delete: true,
      },
    },
    {
      filter: {
        to: 'ops-contact+ops-gitlab-net@gitlab.com',
      },
      actions: {
        delete: true,
        markSpam: false,
        markImportant: false,
        labels: [
          'OpsGitLabInstance',
        ],
      },
    },
    {
      filter: {
        and: [
          {
            to: '-ops-notifications@gitlab.com, -ops-contact+ops-gitlab-net@gitlab.com',
            isEscaped: true,
          },
          {
            query: 'list:<ops-contact.gitlab.com>',
          },
        ],
      },
      actions: {
        archive: true,
        markImportant: false,
        labels: [
          'Ops Contact',
        ],
      },
    },
    {
      filter: {
        subject: 'yum update',
        isEscaped: true,
      },
      actions: {
        archive: true,
        labels: [
          'Patching',
        ],
      },
    },
    {
      filter: {
        subject: 'apt-get update',
        isEscaped: true,
      },
      actions: {
        archive: true,
        labels: [
          'Patching',
        ],
      },
    },
    {
      filter: {
        subject: 'apt-mark',
      },
      actions: {
        archive: true,
        labels: [
          'Patching',
        ],
      },
    },
    {
      filter: {
        to: 'ops-contact+letsencrypt@gitlab.com',
      },
      actions: {
        delete: true,
      },
    },
    {
      filter: {
        from: 'no-reply@dtdg.co',
      },
      actions: {
        delete: true,
      },
    },
  ],
}
